Instrucciones de la línea de comando
También puede subir archivos existentes desde su ordenador utilizando las instrucciones que se muestran a continuación.


Configuración global de Git
git config --global user.name "Kevin Hidalgo"
git config --global user.email "kevin.hidalgo@predictiva.com.co"

Crear un nuevo repositorio
git clone https://gitlab.com/Kevin.Hidalgo.Predictiva/img.git
cd img
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Push a una carpeta existente
cd existing_folder
git init
git remote add origin https://gitlab.com/Kevin.Hidalgo.Predictiva/img.git
git add .
git commit -m "Initial commit"
git push -u origin master

Push a un repositorio de Git existente
cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/Kevin.Hidalgo.Predictiva/img.git
git push -u origin --all
git push -u origin --tags